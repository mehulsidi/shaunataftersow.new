﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Shaunataftersow.Models
{
    public class Activity:GenericItem
    {
        public List<ActivityTask> Tasks { get; set; } =new List<ActivityTask>();
    }
}
