﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Shaunataftersow.Models
{
     public  class TaskControl:EntityWithDate
    {
        [ForeignKey(nameof(Task))]
        public int TaskId { get; set; }

        [ForeignKey(nameof(Control))]
        public int ControlId { get; set; }

        [ForeignKey(nameof(TaskId))]
        public Task Task { get; set; }

        [ForeignKey(nameof(ControlId))]
        public Control Control { get; set; }

        [ForeignKey(nameof(Organisation))]
        public int? OrganisationId { get; set; }

        [ForeignKey(nameof(OrganisationId))]
        public Organisation Organisation { get; set; }
    }
}
