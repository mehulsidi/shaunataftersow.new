﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Shaunataftersow.Models
{
    public class ActivityTask:EntityWithDate
    {
        [ForeignKey(nameof(Activity))]
        public int ActivityId { get; set; }

        [ForeignKey(nameof(Task))]
        public int TaskId { get; set; }

        [ForeignKey(nameof(ActivityId))]
        public Activity Activity { get; set; }

        [ForeignKey(nameof(TaskId))]
        public Task Task { get; set; }

        [ForeignKey(nameof(Organisation))]
        public int? OrganisationId { get; set; }

        [ForeignKey(nameof(OrganisationId))]
        public Organisation Organisation { get; set; }
    }
}


