﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Shaunataftersow.Models
{
     public  class TaskHazard:EntityWithDate
    {
        [ForeignKey(nameof(Hazard))]
        public int HazardId { get; set; }

        [ForeignKey(nameof(Task))]
        public int TaskId { get; set; }

        [ForeignKey(nameof(HazardId))]
        public Hazard Hazard { get; set; }

        [ForeignKey(nameof(TaskId))]
        public Task Task { get; set; }

        [ForeignKey(nameof(Organisation))]
        public int? OrganisationId { get; set; }

        [ForeignKey(nameof(OrganisationId))]
        public Organisation Organisation { get; set; }
    }
}
