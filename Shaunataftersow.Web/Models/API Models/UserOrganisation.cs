﻿using Shaunataftersow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Shaunataftersow.Web.Models
{
    public class UserOrganisation : EntityWithDate
    {
        public string UserId { get; set; }

        public string UserEmail { get; set; }
        public string UserName { get; set; }

        public int RoleId { get; set; }

        public string Mobile { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        [ForeignKey(nameof(Organisation))]
        public int OrganisationId { get; set; }

        [ForeignKey(nameof(OrganisationId))]
        public Organisation Organisation { get; set; }
    }
}