﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shaunataftersow.Web.Models
{
    public class UserOrganizationModel
    {
        public string UserId { get; set; }
        public int Id { get; set; }

        public string UserEmail { get; set; }

       // [Required(ErrorMessage = "User name is required.")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Role is required.")]
        [Display(Name = "Role")]
        public int RoleId { get; set; }

        [Required(ErrorMessage = "Mobile no is required.")]
        [Display(Name = "Mobile No")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public bool Active { get; set; }
    }
}