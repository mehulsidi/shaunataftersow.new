﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shaunataftersow.Web.Models
{
    public class ControlModel
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Description name is required.")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Type name is required.")]
        [Display(Name = "Type")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Icon is required.")]
        [Display(Name = "Icon")]
        public string Icon { get; set; }
    }
}