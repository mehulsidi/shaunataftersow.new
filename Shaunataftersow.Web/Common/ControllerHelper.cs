﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shaunataftersow.Web.Common
{
    public class ControllerHelper
    {
        public const string User = "User";
        public const string Organization = "Organization";
        public const string Control = "Control";
        public const string Hazard = "Hazard";
        public const string Task = "Task";
        public const string Activity = "Activity";
        public const string UserOrganisation = "UserOrganisation";
    }
}