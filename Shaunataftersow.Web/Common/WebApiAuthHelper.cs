﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Shaunataftersow.Web.Common
{
    public class WebApiAuthHelper
    {
        private string app_id = "6F609861-AAF0-45BA-A50C-65F5F1A57510";
        private string api_key = "AiFWTGEtkFIce2htaPe4HRbdRed2Gki42Ih0hfV83u4=";

        public AuthenticationHeaderValue GenerateToken(string requestUri, string requestHttpMethod)
        {
            string requestSignatureBase64String = string.Empty;

            requestUri = HttpUtility.UrlEncode(requestUri.ToLower());            

            //Calculate UNIX time
            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = DateTime.UtcNow - epochStart;
            string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

            //create random nonce for each request
            string nonce = Guid.NewGuid().ToString("N");

            string signatureRawData = String.Format("{0}{1}{2}{3}{4}", app_id, requestHttpMethod, requestUri, requestTimeStamp, nonce);

            var secretKeyByteArray = Convert.FromBase64String(api_key);

            byte[] signature = Encoding.UTF8.GetBytes(signatureRawData);

            using (HMACSHA256 hmac = new HMACSHA256(secretKeyByteArray))
            {
                byte[] signatureBytes = hmac.ComputeHash(signature);
                requestSignatureBase64String = Convert.ToBase64String(signatureBytes).TrimEnd('=');                
            }

            var token = new AuthenticationHeaderValue("token", string.Format("{0}:{1}:{2}:{3}", app_id, requestSignatureBase64String, nonce, requestTimeStamp));
            var token1 = new AuthenticationHeaderValue("Basic", "d2VidXNlcjoocUM2ZG1VM0BEIUc=");
            return token1;
        }
    }
}