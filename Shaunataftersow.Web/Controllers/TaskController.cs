﻿using Shaunataftersow.Models;
using Shaunataftersow.Web.Common;
using Shaunataftersow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Shaunataftersow.Web.Controllers
{
    public class TaskController : Controller
    {
        public ActionResult Index()
        {
            return View(ViewHelper.Task);
        }
        public async Task<ActionResult> OpenPopUp(int Id)
        {
            ReadHazardList();
            TaskModel objModel = new TaskModel();
            objModel.Icon = "icon.png";

            if (Id > 0)
            {
                var result = new Shaunataftersow.Models.Task();
                var uri = "Tasks/" + Id;
                result = await WebApiHelper.HttpClientRequestReponce(result, uri);
                if (result != null)
                {
                    objModel.Description = result.Description;
                    objModel.Type = result.Type;
                    objModel.Icon = result.Icon;
                    objModel.HazardIds = GetTaskHazard(Id);
                    objModel.id = Id;
                }
            }

            return View(ViewHelper.AddEditTask, objModel);
        }
        public async Task<ActionResult> SaveTask(HttpPostedFileBase file, TaskModel model)
        {
            string Flag = string.Empty;
            if (file != null)
            {
                string path = Server.MapPath(WebConfigurationManager.AppSettings["TaskPath"]);
                bool folderExists = System.IO.Directory.Exists(path);
                if (!folderExists)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                string FileName = DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("hhmmssfff") + file.FileName.Split('.')[0].ToString() + fileExtension;
                FileName = FileName.Replace(" ", "_");
                file.SaveAs(path + FileName);
                model.Icon = string.IsNullOrEmpty(FileName) ? "" : FileName;
            }



            Shaunataftersow.Models.Task objTask = new Shaunataftersow.Models.Task();
            objTask.Description = model.Description;
            objTask.Type = model.Type;
            objTask.Icon = model.Icon;
            objTask.Hazards = new List<TaskHazard>();
            if (!string.IsNullOrEmpty(model.HazardIds))
            {

                foreach (var item in model.HazardIds.Split(','))
                {
                    TaskHazard objHazard = new TaskHazard();
                    objHazard.HazardId = Convert.ToInt32(item);
                    objTask.Hazards.Add(objHazard);
                }
            }

            if (model.id == 0)
            {
                Flag = await WebApiHelper.HttpClientPostPassEntityReturnString<Shaunataftersow.Models.Task>(objTask, "Tasks");
            }
            else
            {
                objTask.Id = model.id;
                Flag = await WebApiHelper.HttpClientPutPassEntityReturnString<Shaunataftersow.Models.Task>(objTask, "Tasks/" + model.id);
            }
            //return RedirectToAction("Index", TasklerHelper.Task);
            return RedirectToAction("Index", ControllerHelper.Task);
        }
        public ActionResult DeleteTask(int id)
        {
            string Flag = string.Empty;
            Flag = WebApiHelper.HttpDeleteClientRequestReponceSync("Tasks/" + id);

            return Json(new { Flag = Flag }, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> ReadTaskList()
        {
            var result = new List<TaskModel>();
            var uri = "Tasks";
            result = await WebApiHelper.HttpClientRequestReponce(result, uri);

            if (result.Count > 0)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", ControllerHelper.Task);
            }
        }
        public void ReadHazardList()
        {
            var result = new List<HazardModel>();
            var uri = "Hazards";
            result = WebApiHelper.HttpClientRequestReponceSync(result, uri);
            IEnumerable<SelectListItem> items = result
           .Select(c => new SelectListItem
           {
               Value = c.id.ToString(),
               Text = c.Description
           });
            ViewBag.HazardList = items;
        }
        public string GetTaskHazard(int TaskId)
        {
            string controlsId = string.Empty;
            var result = new List<TaskHazard>();
            var uri = "TaskHazards";
            result = WebApiHelper.HttpClientRequestReponceSync(result, uri);
            controlsId = string.Join(",", result.Where(x => x.TaskId == TaskId).Select(x => x.HazardId));
            return controlsId;
        }

    }
}