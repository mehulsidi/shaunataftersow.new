﻿using Shaunataftersow.Models;
using Shaunataftersow.Web.Common;
using Shaunataftersow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Shaunataftersow.Web.Controllers
{
    public class UserOrganisationController : Controller
    {
        public ActionResult Index()
        {
            return View(ViewHelper.UserOrganisation);
        }
        public async Task<ActionResult> OpenPopUp(int Id)
        {
            UserOrganizationModel objModel = new UserOrganizationModel();
            if (Id > 0)
            {
                var result = new UserOrganizationModel();
                var uri = "UserOrganisations/" + Id;
                result = await WebApiHelper.HttpClientRequestReponce(result, uri);
                objModel = result;
                objModel.Id = Id;
            }
            ReadRoleList();
            return View(ViewHelper.AddEditUserOrganisation, objModel);
        }
        public async Task<ActionResult> SaveUserOrganisation(HttpPostedFileBase file, UserOrganizationModel model)
        {
            string Flag = string.Empty;
            try
            {
                Flag = await WebApiHelper.HttpClientPutPassEntityReturnString<UserOrganizationModel>(model, "UserOrganisations/" + model.Id);
            }
            catch (Exception exd)
            {

                Flag = exd.Message.ToString();
            }
                
            
            return RedirectToAction("Index", ControllerHelper.UserOrganisation);
        }
        public ActionResult DeleteUserOrganisation(int id)
        {
            string Flag = string.Empty;
            Flag = WebApiHelper.HttpDeleteClientRequestReponceSync("UserOrganisations/" + id);

            return Json(new { Flag = Flag }, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> ReadUserOrganisationList()
        {
            var result = new List<UserOrganizationModel>();
            var uri = "UserOrganisations";
            result = await WebApiHelper.HttpClientRequestReponce(result, uri);

            if (result.Count > 0)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", ControllerHelper.UserOrganisation);
            }
        }
        public void ReadRoleList()
        {
            var result = new List<UserRoleModel>();
            var uri = "users/roles";
            result = WebApiHelper.HttpClientRequestReponceSync(result, uri);
            IEnumerable<SelectListItem> items = result
           .Select(c => new SelectListItem
           {
               Value = c.id.ToString(),
               Text = c.name
           });
            ViewBag.RoleList = items;
        }
    }
}