﻿using Shaunataftersow.Models;
using Shaunataftersow.Web.Common;
using Shaunataftersow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Shaunataftersow.Web.Controllers
{
    public class ActivityController : Controller
    {
        public ActionResult Index()
        {
            return View(ViewHelper.Activity);
        }
        public async Task<ActionResult> OpenPopUp(int Id)
        {
            ReadTaskList();
            ActivityModel objModel = new ActivityModel();
            objModel.Icon = "icon.png";

            if (Id > 0)
            {
                var result = new Shaunataftersow.Models.Activity();
                var uri = "Activities/" + Id;
                result = await WebApiHelper.HttpClientRequestReponce(result, uri);
                if (result != null)
                {
                    objModel.Description = result.Description;
                    objModel.Type = result.Type;
                    objModel.Icon = result.Icon;
                    objModel.TaskIds = GetActivityTask(Id);
                    objModel.id = Id;
                }
            }

            return View(ViewHelper.AddEditActivity, objModel);
        }
        public async Task<ActionResult> SaveActivity(HttpPostedFileBase file, ActivityModel model)
        {
            string Flag = string.Empty;
            if (file != null)
            {
                string path = Server.MapPath(WebConfigurationManager.AppSettings["ActivityPath"]);
                bool folderExists = System.IO.Directory.Exists(path);
                if (!folderExists)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                string FileName = DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("hhmmssfff") + file.FileName.Split('.')[0].ToString() + fileExtension;
                FileName = FileName.Replace(" ", "_");
                file.SaveAs(path + FileName);
                model.Icon = string.IsNullOrEmpty(FileName) ? "" : FileName;
            }



            Shaunataftersow.Models.Activity objActivity = new Shaunataftersow.Models.Activity();
            objActivity.Description = model.Description;
            objActivity.Type = model.Type;
            objActivity.Icon = model.Icon;
            objActivity.Tasks = new List<ActivityTask>();
            if (!string.IsNullOrEmpty(model.TaskIds))
            {

                foreach (var item in model.TaskIds.Split(','))
                {
                    ActivityTask objActivityTask = new ActivityTask();
                    objActivityTask.TaskId= Convert.ToInt32(item);
                    objActivity.Tasks.Add(objActivityTask);
                }
            }

            if (model.id == 0)
            {
                Flag = await WebApiHelper.HttpClientPostPassEntityReturnString<Shaunataftersow.Models.Activity>(objActivity, "Activities");
            }
            else
            {
                objActivity.Id = model.id;
                Flag = await WebApiHelper.HttpClientPutPassEntityReturnString<Shaunataftersow.Models.Activity>(objActivity, "Activities/" + model.id);
            }
            //return RedirectToAction("Index", ActivitylerHelper.Activity);
            return RedirectToAction("Index", ControllerHelper.Activity);
        }
        public ActionResult DeleteActivity(int id)
        {
            string Flag = string.Empty;
            Flag = WebApiHelper.HttpDeleteClientRequestReponceSync("Activities/" + id);

            return Json(new { Flag = Flag }, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> ReadActivityList()
        {
            var result = new List<ActivityModel>();
            var uri = "Activities";
            result = await WebApiHelper.HttpClientRequestReponce(result, uri);

            if (result.Count > 0)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", ControllerHelper.Activity);
            }
        }
        public void ReadTaskList()
        {
            var result = new List<TaskModel>();
            var uri = "Tasks";
            result = WebApiHelper.HttpClientRequestReponceSync(result, uri);
            IEnumerable<SelectListItem> items = result
           .Select(c => new SelectListItem
           {
               Value = c.id.ToString(),
               Text = c.Description
           });
            ViewBag.TaskList = items;
        }
        public string GetActivityTask(int ActivityId)
        {
            string controlsId = string.Empty;
            var result = new List<ActivityTask>();
            var uri = "ActivityTasks";
            result = WebApiHelper.HttpClientRequestReponceSync(result, uri);
            controlsId = string.Join(",", result.Where(x => x.ActivityId == ActivityId).Select(x => x.TaskId));
            return controlsId;
        }
    }
}