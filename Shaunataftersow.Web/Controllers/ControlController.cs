﻿using Shaunataftersow.Web.Common;
using Shaunataftersow.Web.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Shaunataftersow.Web.Controllers
{
    public class ControlController : Controller
    {
        public ActionResult Index()
        {
            return View(ViewHelper.Control);
        }
        public async Task<ActionResult> OpenPopUp(int Id)
        {
            ControlModel objModel = new ControlModel();
            objModel.Icon = "icon.png";
            if (Id > 0)
            {
                var result = new ControlModel();
                var uri = "Controls/" + Id;
                result = await WebApiHelper.HttpClientRequestReponce(result, uri);
                objModel = result;
                objModel.id = Id;
            }
            return View(ViewHelper.AddEditControl, objModel);
        }
        public async Task<ActionResult> SaveControl(HttpPostedFileBase file,ControlModel model)
        {
            string Flag = string.Empty;
            if (file != null)
            {
                    string path = Server.MapPath(WebConfigurationManager.AppSettings["ControlPath"]);
                    bool folderExists = System.IO.Directory.Exists(path);
                    if (!folderExists)
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    string fileExtension = System.IO.Path.GetExtension(file.FileName);
                    string FileName = DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("hhmmssfff") + file.FileName.Split('.')[0].ToString()+fileExtension;
                FileName = FileName.Replace(" ", "_");
                    file.SaveAs(path + FileName);
                    model.Icon = string.IsNullOrEmpty(FileName) ? "" : FileName;
            }

            if (model.id == 0)
            {
                Flag = await WebApiHelper.HttpClientPostPassEntityReturnString<ControlModel>(model, "Controls");
            }
            else
            {
                Flag = await WebApiHelper.HttpClientPutPassEntityReturnString<ControlModel>(model, "Controls/" + model.id);
            }
           return RedirectToAction("Index", ControllerHelper.Control);
        }
        public ActionResult DeleteControl(int id)
        {
            string Flag = string.Empty;
            Flag = WebApiHelper.HttpDeleteClientRequestReponceSync("Controls/" + id);

            return Json(new { Flag = Flag }, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> ReadControlList()
        {
            var result = new List<ControlModel>();
            var uri = "Controls";
            result = await WebApiHelper.HttpClientRequestReponce(result, uri);

            if (result.Count > 0)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", ControllerHelper.Control);
            }
        }
    }
}