﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Shaunataftersow.Web.Common;
using Shaunataftersow.Web.Models;

namespace Shaunataftersow.Web.Controllers
{
    public class OrganizationController : Controller
    {
        // GET: Organization
        public ActionResult Index()
        {
            return View(ViewHelper.Organization);
        }
        public async Task<ActionResult> OpenPopUp(int Id)
        {
            OrganizationModel objModel = new OrganizationModel();
            if (Id > 0)
            {
                var result = new OrganizationModel();
                var uri = "Organisations/" + Id;
                result = await WebApiHelper.HttpClientRequestReponce(result, uri);
                objModel = result;
            }
            return View(ViewHelper.AddEditOrganization, objModel);
        }
        public async Task<ActionResult> SaveOrganization(OrganizationModel model)
        {
            string Flag = string.Empty;
            dynamic expando = new ExpandoObject();
            if (model.id == 0)
            {
                Flag = await WebApiHelper.HttpClientPostPassEntityReturnString<OrganizationModel>(model, "Organisations");
            }
            else
            {
                Flag = await WebApiHelper.HttpClientPutPassEntityReturnString<OrganizationModel>(model, "Organisations/" + model.id);
            }
            //return RedirectToAction("Index", ControllerHelper.Organization);
            return Json(new { Flag = Flag }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteOrganization(int id)
        {
            string Flag = string.Empty;
            Flag = WebApiHelper.HttpDeleteClientRequestReponceSync("Organisations/"+id);

            return Json(new { Flag = Flag }, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> ReadOrganizationList()
        {
            var result = new List<OrganizationModel>();
            var uri = "Organisations";
            result = await WebApiHelper.HttpClientRequestReponce(result, uri);

            if (result.Count > 0)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", ControllerHelper.Organization);
            }
        }
    }
}